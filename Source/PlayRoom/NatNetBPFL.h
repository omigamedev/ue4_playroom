// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "NatNet.h"
#include "NatNetBPFL.generated.h"

UCLASS()
class UNatNetBPFL : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category = NatNet)
	static void RequestDescription();
	UFUNCTION(BlueprintCallable, Category = NatNet)
	static FRigidBody GetRigidBody(FName name);
};
