// Fill out your copyright notice in the Description page of Project Settings.

#include "NatNetPCH.h"
#include "NatNetChar.h"
#include "IHeadMountedDisplay.h"


// Sets default values
ANatNetChar::ANatNetChar()
{
	RigidBodyName = "gearvr";
}

// Called to bind functionality to input
void ANatNetChar::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
	InputComponent->BindAction("Tap", IE_Pressed, this, &ThisClass::OnTap);
}

void ANatNetChar::CalcCamera(float DeltaTime, struct FMinimalViewInfo& OutResult)
{
	Super::CalcCamera(DeltaTime, OutResult);

	auto rb = FNatNetModule::GetRigidBody(RigidBodyName);
	//auto pos = OptitrackNativeModule::GetRigidBodyPos();
	//auto rot = OptitrackNativeModule::GetRigidBodyRot();

	if (rb.valid)
	{
		OutResult.Location = rb.pos * FVector(1.1, 1.1, 1)/* + FVector(0, 0, 100)*/;
		if (GEngine && !GEngine->HMDDevice.IsValid())
		{
			OutResult.Rotation = FRotator(rb.quat);
		}
	}
	else
	{
		OutResult.Location = FVector(0, 0, 150);

	}
}

void ANatNetChar::OnTap()
{
	if (GEngine && GEngine->HMDDevice.IsValid())
	{
		auto rb = FNatNetModule::GetRigidBody(RigidBodyName);
		auto rot = FRotator(rb.quat);
		GEngine->HMDDevice->ResetOrientationAndPosition(rot.Yaw);
		LOG("OnTap");
	}
	else
		LOG("OnTap Failed");
}

