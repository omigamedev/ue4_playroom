#pragma once

#include "NatNet.h"
#include "Engine.h"
#include "Networking.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Runtime/Launch/Resources/Version.h"

DECLARE_LOG_CATEGORY_EXTERN(NatNetLog, Log, All);
#define LOG(M,...) UE_LOG(NatNetLog, Warning, TEXT(M), ##__VA_ARGS__); \
	if(GEngine) GEngine->AddOnScreenDebugMessage(-1, 1, FColor::Red, FString::Printf(TEXT(M), ##__VA_ARGS__));
