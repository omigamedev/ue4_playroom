// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "NatNetChar.generated.h"

UCLASS()
class PLAYROOM_API ANatNetChar : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ANatNetChar();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = NatNet)
	FName RigidBodyName;

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	virtual void CalcCamera(float DeltaTime, struct FMinimalViewInfo& OutResult) override;
	
	void OnTap();
};
