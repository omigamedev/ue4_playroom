// Fill out your copyright notice in the Description page of Project Settings.

#include "NatNetPCH.h"
#include "NatNetBPFL.h"

void UNatNetBPFL::RequestDescription()
{
	FNatNetModule::RequestDescription();
}

FRigidBody UNatNetBPFL::GetRigidBody(FName name)
{
	return FNatNetModule::GetRigidBody(name);
}
