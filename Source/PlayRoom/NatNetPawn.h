#pragma once

#include "GameFramework/Pawn.h"
#include "NatNetPawn.generated.h"

UCLASS()
class ANatNetPawn : public APawn
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = NatNet)
	FName RigidBodyName;

	ANatNetPawn(const FObjectInitializer& init);
	virtual void CalcCamera(float DeltaTime, struct FMinimalViewInfo& OutResult) override;
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	virtual void Tick(float DeltaSeconds) override;
	void OnTap();
};
