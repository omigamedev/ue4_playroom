#include "NatNetPCH.h"
#include "NatNetPawn.h"
#include "IHeadMountedDisplay.h"

ANatNetPawn::ANatNetPawn(const FObjectInitializer& init) : Super(init)
{
	PrimaryActorTick.bCanEverTick = true;
	RigidBodyName = "gearvr";
}

void ANatNetPawn::CalcCamera(float DeltaTime, struct FMinimalViewInfo& OutResult)
{
	Super::CalcCamera(DeltaTime, OutResult);

	auto rb = FNatNetModule::GetRigidBody(RigidBodyName);
	//auto pos = OptitrackNativeModule::GetRigidBodyPos();
	//auto rot = OptitrackNativeModule::GetRigidBodyRot();

	if (rb.valid)
	{
		OutResult.Location = rb.pos * FVector(1.1, 1.1, 1)/* + FVector(0, 0, 100)*/;
		if (GEngine && !GEngine->HMDDevice.IsValid())
		{
			OutResult.Rotation = FRotator(rb.quat);
		}
	}
}

void ANatNetPawn::SetupPlayerInputComponent(UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
	InputComponent->BindAction("Tap", IE_Pressed, this, &ThisClass::OnTap);
}

void ANatNetPawn::OnTap()
{
	if (GEngine && GEngine->HMDDevice.IsValid())
	{
		auto rb = FNatNetModule::GetRigidBody(RigidBodyName);
		auto rot = FRotator(rb.quat);
		GEngine->HMDDevice->ResetOrientationAndPosition(rot.Yaw);
		LOG("OnTap");
	}
	else
		LOG("OnTap Failed");
}

void ANatNetPawn::Tick(float DeltaSeconds)
{
	static float elapsed = 0;
	elapsed += DeltaSeconds;

	if (elapsed > 10)
	{
		//OnTap();
		elapsed = 0;
	}
}
