// Some copyright should be here...

#pragma once

#include "Networking.h"
#include "NatNet.generated.h"

USTRUCT()
struct FRigidBody
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = NatNet)
	FQuat quat;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = NatNet)
	FRotator rot;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = NatNet)
	FVector pos;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = NatNet)
	FString name;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = NatNet)
	float err;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = NatNet)
	bool valid;
	
	FRigidBody() : quat(EForceInit::ForceInitToZero), pos(0), err(0), valid(false) {}
	FRigidBody(FString name) : name(name) {}

	FTransform GetTransform() { return FTransform(rot, pos); }
};

class FSocketListener : public FRunnable
{
	FRunnableThread* Thread;
	FSocket* CommandSocket;
	FSocket* DataSocket;
	FIPv4Address CommandEP;
	bool running;
public:
	FSocketListener();
	virtual uint32 Run();
	virtual void Stop();
	void Unpack(char* data);
	void RequestDescription();
};

class FNatNetModule : public IModuleInterface
{
public:
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
	static FRigidBody GetRigidBody(FName name);
	static void RequestDescription();
};