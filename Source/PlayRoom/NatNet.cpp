// Some copyright should be here...

#include "NatNetPCH.h"
#include "NatNet.h"

#define LOCTEXT_NAMESPACE "FNatNetModule"

#define MAX_NAMELENGTH              256

// NATNET message ids
#define NAT_PING                    0 
#define NAT_PINGRESPONSE            1
#define NAT_REQUEST                 2
#define NAT_RESPONSE                3
#define NAT_REQUEST_MODELDEF        4
#define NAT_MODELDEF                5
#define NAT_REQUEST_FRAMEOFDATA     6
#define NAT_FRAMEOFDATA             7
#define NAT_MESSAGESTRING           8
#define NAT_UNRECOGNIZED_REQUEST    100
#define UNDEFINED                   999999.9999

#define MAX_PACKETSIZE				100000	// max size of packet (actual packet size is dynamic)

// sender
typedef struct
{
	char szName[MAX_NAMELENGTH];            // sending app's name
	unsigned char Version[4];               // sending app's version [major.minor.build.revision]
	unsigned char NatNetVersion[4];         // sending app's NatNet version [major.minor.build.revision]

} sSender;

typedef struct
{
	unsigned short iMessage;                // message ID (e.g. NAT_FRAMEOFDATA)
	unsigned short nDataBytes;              // Num bytes in payload
	union
	{
		unsigned char  cData[MAX_PACKETSIZE];
		char           szData[MAX_PACKETSIZE];
		unsigned long  lData[MAX_PACKETSIZE / 4];
		float          fData[MAX_PACKETSIZE / 4];
		sSender        Sender;
	} Data;                                 // Payload

} sPacket;

#define MULTICAST_ADDRESS		"239.255.42.99"     // IANA, local network
#define PORT_COMMAND            1510
#define PORT_DATA  			    1511                

int NatNetVersion[4] = { 0, 0, 0, 0 };
int ServerVersion[4] = { 0, 0, 0, 0 };
int bDataDefined = false;
TMap<int, FName> idsMap;
TMap<FName, FRigidBody> namesMap;
FCriticalSection MarkersMutex;
FSocketListener* SocketListener = nullptr;

struct PacketReader
{
	char* data;
	int index;
	PacketReader(char* data) : data(data), index(0) {}
	char* ReadString()
	{
		char* ret = data + index;
		index += strlen(ret) + 1;
		return ret;
	}
	char ReadInt8()
	{
		char ret = *(data + index);
		index += 1;
		return ret;
	}
	short ReadInt16()
	{
		short ret = 0;
		memcpy(&ret, data + index, 2);
		index += 2;
		return ret;
	}
	int ReadInt32()
	{
		int ret = 0;
		memcpy(&ret, data + index, 4);
		index += 4;
		return ret;
	}
	float ReadFloat()
	{
		float ret = 0;
		memcpy(&ret, data + index, 4);
		index += 4;
		return ret;
	}
	FVector ReadVector()
	{
		float x = ReadFloat(),
			y = ReadFloat(),
			z = ReadFloat();
		return FVector(x, z, y);
	}
	FQuat ReadQuat()
	{
		float x = ReadFloat(),
			y = ReadFloat(),
			z = ReadFloat(),
			w = ReadFloat();
		return FQuat(-x, -z, -y, w);
	}
	void Skip(int nbytes) { index += nbytes; }
};

void FSocketListener::Unpack(char* data)
{
	// See also : https://github.com/rocketman768/NatNetLinux

	PacketReader pr(data);
	int major = 2;
	int minor = 7;
	char name[MAX_NAMELENGTH];

	int MessageID = pr.ReadInt16();
	int nBytes = pr.ReadInt16();
	switch (MessageID)
	{
	case NAT_FRAMEOFDATA:
	{
		int frameID = pr.ReadInt32();
		int nMarkersSet = pr.ReadInt32();
		for (int i = 0; i < nMarkersSet; i++)
		{
			pr.ReadString();
			int nMarkers = pr.ReadInt32();
			for (int j = 0; j < nMarkers; j++)
			{
				FVector pos = pr.ReadVector();
			}
		}
		// unidentified markers
		int nOtherMarkers = pr.ReadInt32();
		for (int i = 0; i < nOtherMarkers; i++)
		{
			FVector pos = pr.ReadVector();
		}
		// rigid bodies
		int nRigidBodies = pr.ReadInt32();
		MarkersMutex.Lock();
		for (int i = 0; i < nRigidBodies; i++)
		{
			// rigid body pos/ori
			int ID = pr.ReadInt32();
			FVector pos = pr.ReadVector();
			FQuat   rot = pr.ReadQuat();

			// associated marker positions
			int nRigidMarkers = pr.ReadInt32();
			int nBytes = nRigidMarkers * 3 * sizeof(float);
			pr.Skip(nBytes);

			if (major >= 2)
			{
				// associated marker IDs
				nBytes = nRigidMarkers*sizeof(int);
				pr.Skip(nBytes);

				// associated marker sizes
				nBytes = nRigidMarkers*sizeof(float);
				pr.Skip(nBytes);
			}

			float fError = 0;;
			if (major >= 2)
			{
				// Mean marker error
				fError = pr.ReadFloat();
			}

			// 2.6 and later
			bool bTrackingValid = false;
			if (((major == 2) && (minor >= 6)) || (major > 2) || (major == 0))
			{
				// params
				short params = pr.ReadInt16();
				bTrackingValid = params & 0x01; // 0x01 : rigid body was successfully tracked in this frame
			}

			auto map = idsMap.Find(ID);
			if (map)
			{
				FRigidBody& rb = namesMap[*map];
				rb.pos = pos * FVector(133, 133, 100);
				rb.quat = rot;
				rb.rot = FRotator(rot);
				rb.err = fError;
				rb.valid = bTrackingValid;
			}

			//LOG(TEXT("ID:%d pos:[%3.2f,%3.2f,%3.2f] ori:[%3.2f,%3.2f,%3.2f,%3.2f] markers:%d err:%3.2f tracked:%d"), 
			//	ID, pos.X, pos.Y, pos.Z, rot.X, rot.Y, rot.Z, rot.W, nRigidMarkers, fError, bTrackingValid);
			//LOG(TEXT("ID:%d pos:[%3.2f,%3.2f,%3.2f]"), ID, pos.X, pos.Y, pos.Z);
		}
		MarkersMutex.Unlock();
	}
	break;
	case NAT_MODELDEF:
	{
		int nDatasets = pr.ReadInt32();
		for (int i = 0; i < nDatasets; i++)
		{
			int type = pr.ReadInt32();
			if (type == 0) // markerset
			{
				pr.ReadString();
				int nMarkers = pr.ReadInt32();
				for (int j = 0; j < nMarkers; j++)
				{
					pr.ReadString();
				}
			}
			else if (type == 1) // rigid body
			{
				name[0] = '\0';
				if (major >= 2)
				{
					strcpy(name, pr.ReadString()); // name
				}

				int ID = pr.ReadInt32();

				LOG("RigidBody %d %s", ID, ANSI_TO_TCHAR(name));
				namesMap.FindOrAdd(FName(name)) = FRigidBody(name);
				idsMap.FindOrAdd(ID) = FName(name);

				int parentID = pr.ReadInt32();
				float xoffset = pr.ReadInt32();
				float yoffset = pr.ReadInt32();
				float zoffset = pr.ReadInt32();
			}
			else if (type == 2) // skeleton
			{
				pr.ReadString();

				int ID = pr.ReadInt32();
				int nRigidBodies = pr.ReadInt32();

				for (int j = 0; j < nRigidBodies; j++)
				{
					if (major >= 2)
					{
						pr.ReadString(); // RB name
					}

					int ID = pr.ReadInt32();
					int parentID = pr.ReadInt32();
					float xoffset = pr.ReadInt32();
					float yoffset = pr.ReadInt32();
					float zoffset = pr.ReadInt32();
				}
			}
		}
		bDataDefined = true;
	}
	break;
	case NAT_PINGRESPONSE:
	{
		sPacket* packet = (sPacket*)data;
		for (int i = 0; i < 4; i++)
		{
			NatNetVersion[i] = (int)packet->Data.Sender.NatNetVersion[i];
			ServerVersion[i] = (int)packet->Data.Sender.Version[i];
		}
	}
	break;
	default:
	{
		int n = pr.ReadInt32();
	}
	break;
	}
}

uint32 FSocketListener::Run()
{
	static auto* SSS = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM);

	CommandEP = FIPv4Address(192, 168, 127, 87);
	auto MulticastEP = FIPv4Address(239, 255, 42, 99);
	auto BindEP = FIPv4Address::Any;

	// Retrieve config
	FString ValueReceived;
	TArray<FString> parts;
	
	if (GConfig->GetString(TEXT("NatNet"), TEXT("ServerIP"), ValueReceived, GGameIni))
	{
#if ENGINE_MINOR_VERSION > 7
		ValueReceived.ParseIntoArray(parts, TEXT("."));
#else
		ValueReceived.ParseIntoArray(&parts, TEXT("."), true);
#endif
		CommandEP = FIPv4Address(FCString::Atoi(*parts[0]), FCString::Atoi(*parts[1]),
			FCString::Atoi(*parts[2]), FCString::Atoi(*parts[3]));
	}
	
	if (GConfig->GetString(TEXT("NatNet"), TEXT("Multicast"), ValueReceived, GGameIni))
	{
#if ENGINE_MINOR_VERSION > 7
		ValueReceived.ParseIntoArray(parts, TEXT("."));
#else
		ValueReceived.ParseIntoArray(&parts, TEXT("."), true);
#endif
		MulticastEP = FIPv4Address(FCString::Atoi(*parts[0]), FCString::Atoi(*parts[1]),
			FCString::Atoi(*parts[2]), FCString::Atoi(*parts[3]));
	}

	auto ServerAddr = SSS->CreateInternetAddr(CommandEP.GetValue(), 1510);
	auto ServerDataAddr = SSS->CreateInternetAddr(CommandEP.GetValue(), 1511);
	auto MulticastAddr = SSS->CreateInternetAddr(MulticastEP.GetValue(), 1511);
	auto AnyDataAddr = SSS->CreateInternetAddr(BindEP.GetValue(), 1511);

	CommandSocket = SSS->CreateSocket(NAME_DGram, TEXT("CommabdSocket"), true);

	DataSocket = SSS->CreateSocket(NAME_DGram, TEXT("DataSocket"), true);
	DataSocket->SetReuseAddr(true);
	if (!DataSocket->Bind(*AnyDataAddr))
		LOG("Error BIND DataSocket");
	DataSocket->JoinMulticastGroup(*MulticastAddr);

	RequestDescription();

	uint8* buffer = new uint8[0x1000];
	uint32 Size;

	while (running)
	{
		while (CommandSocket->HasPendingData(Size))
		{
			int32 Read = 0;
			CommandSocket->Recv(buffer, 0x1000, Read);
			if (Read > 0)
				Unpack((char*)buffer);
		}
		while (DataSocket->HasPendingData(Size))
		{
			int32 Read = 0;
			DataSocket->Recv(buffer, 0x1000, Read);
			if (Read > 0)
			{
				//LOG("Data Bytes Read %d", Read);
				Unpack((char*)buffer);
			}
		}

		FPlatformProcess::Sleep(1.f / 120.f);
	}

	return 0;
}

FSocketListener::FSocketListener()
{
	running = true;
	Thread = FRunnableThread::Create(this, TEXT("FSocketListenerThread"));
}

void FSocketListener::RequestDescription()
{
	static auto* SSS = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM);
	auto ServerAddr = SSS->CreateInternetAddr(CommandEP.GetValue(), 1510);

	sPacket PacketOut{ 0 };
	PacketOut.iMessage = NAT_REQUEST_MODELDEF;
	PacketOut.nDataBytes = 0;
	sprintf(PacketOut.Data.Sender.szName, "UE4_NatNetPlugin");

	int32 sendBytes = 0;
	if (!CommandSocket->SendTo((uint8*)&PacketOut, 4 + PacketOut.nDataBytes, sendBytes, *ServerAddr))
	{
		LOG("Error in RequestDescription SendTo");
	}
}

void FSocketListener::Stop()
{
	running = false;
}

void FNatNetModule::StartupModule()
{
	LOG("NatNet Module Started");
	SocketListener = new FSocketListener();
}

void FNatNetModule::ShutdownModule()
{
	SocketListener->Stop();
}

FRigidBody FNatNetModule::GetRigidBody(FName name)
{
	MarkersMutex.Lock();
	FRigidBody rb = namesMap.FindOrAdd(name);
	MarkersMutex.Unlock();
	return rb;
}

void FNatNetModule::RequestDescription()
{
	SocketListener->RequestDescription();
}

#undef LOCTEXT_NAMESPACE
	
//IMPLEMENT_MODULE(FNatNetModule, NatNet)
DEFINE_LOG_CATEGORY(NatNetLog);
