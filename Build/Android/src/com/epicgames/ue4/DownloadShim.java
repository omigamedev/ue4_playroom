package com.epicgames.ue4;

import com.omigamedev.PlayRoom.OBBDownloaderService;
import com.omigamedev.PlayRoom.DownloaderActivity;


public class DownloadShim
{
	public static OBBDownloaderService DownloaderService;
	public static DownloaderActivity DownloadActivity;
	public static Class<DownloaderActivity> GetDownloaderType() { return DownloaderActivity.class; }
}

